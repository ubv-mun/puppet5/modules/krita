# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include krita::install
class krita::install {
  package { $krita::package_name:
    ensure => $krita::package_ensure,
  }
}
